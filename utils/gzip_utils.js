"use strict";
/*global Sensor,Device*/


var app = require('../app'),
  parseBytes = require('bytes'),
  zlib = require('zlib');


/**
 * Gzip middleware
 */

function noop(req, res, next) {
  next();
}

var _limit = function (bytes) {
  if ('string' === typeof bytes) {parseBytes(bytes); }
  if ('number' !== typeof bytes) {throw new Error('limit() bytes required'); }
  return function limit(req, res, next) {
    var received = 0,
      len = req.headers['content-length']
        ? parseInt(req.headers['content-length'], 10)
        : null;

    // self-awareness
    if (req._limit) {return next(); }
    req._limit = true;

    // limit by content-length
    if (len && len > bytes) {return next(413); }

    // limit
    req.on('data', function(chunk) {
      received += chunk.length;
      if (received > bytes) {req.destroy(); }
    });

    next();
  };
};

var hasBody = function(req) {
  return 'transfer-encoding' in req.headers || 'content-length' in req.headers;
};

var mime = function(req) {
  var str = req.headers['content-type'] || '';
  return str.split(';')[0];
};

exports = module.exports = function (options) {
  var options = options || {}
    , strict = options.strict !== false;

  var limit = options.limit
    ? _limit(options.limit)
    : noop;

  return function json(req, res, next) {
    if (req._body) return next();
    req.body = req.body || {};

    if (!hasBody(req)) return next();

    // check Content-Type
    if ('application/json' != mime(req)) return next();

    // flag as parsed
    req._body = true;

    // parse
    limit(req, res, function(err){
      if (err) return next(err);
      var buf = [];

      req.on('data', function (chunk) {
        buf.push(chunk);
      });

      req.on('end', function () {
        var buffer = Buffer.concat(buf);
        var encoding = res.req.headers['content-encoding'];

        var cb = function (err, decoded) {
          var buf = decoded.toString();
          var first = buf.trim()[0];

          req.setEncoding('utf8');

          if (0 == buf.length) {
            return next(400, 'invalid json, empty body');
          }

          if (strict && '{' != first && '[' != first) return next(400, 'invalid json');
          try {
            req.body = JSON.parse(buf, options.reviver);
            next();
          } catch (err){
            err.body = buf;
            err.status = 400;
            next(err);
          }
        };

        if (encoding === 'gzip') {
          zlib.gunzip(buffer, cb);
        }
        else if (encoding === 'deflate') {
          zlib.inflate(buffer, cb);
        } else {
          cb(null, buffer.toString());
        }
      });
    });
  }
};
