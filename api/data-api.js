'use strict';

var _ = require('underscore'),
  Client = require('node-rest-client').Client;


function Data(path) {
  this.path = path || 'http://localhost:8888/sensors/';
  this.client = new Client();
}

Data.prototype.parse = function(data) {
  return data.map(function(el) {
    return {'at': el.timestamp, 'value': el.value};
  });
};

Data.prototype.post = function(sensorId, data, next) {
  var that = this,
    args = {
      path: {id: sensorId},
      data: data,
      headers: {'Content-Type': 'application/json'}
    };

  if (_.isEmpty(data)) {
    return next(null, {});
  }

  if (_.find(data, function (el) { return 'timestamp' in el; })) {
    args.data = that.parse(data);
  }

  this.client.post(
    that.path + '${id}/data',
    args,
    function () {
      next();
    }
  ).on('error', next);
};

module.exports.Data = Data;
