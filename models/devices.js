"use strict";
/*global Sensor,Device*/


var mongoose = require('mongoose'),
  app = require('../app'),
  Schema = mongoose.Schema;

/**
 * Sensor Schema
 */

var SensorSchema = new Schema({
  devid: String,
  unit: String,
  type: String,
  name: String
});

var DeviceSchema = new Schema({
  serial: String,
  gps: {type: [Number], index: '2d'},
  location: String,
  sensors: [SensorSchema],
  uuid: String
});

DeviceSchema.virtual('zwid').get(function () {
  return this.serial.split(':')[1];
});

var Device = mongoose.model('Device', DeviceSchema);

module.exports = {
  Device: Device
};
