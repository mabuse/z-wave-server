
'use strict';

var request = require('request-json');

(function () {

  var path = '/smartHomeGateway/rest/v2/data',
    client = request.newClient('http://localhost:8080/'),
    data = require('../test/data/data-sensor-sample.json');

  client.post(path, data, function(err, res, body) {
    return console.log(res.statusCode);
  });
}());
