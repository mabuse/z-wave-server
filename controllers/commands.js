
'use strict';

var app = require('../app'),
  async = require('async'),
  _ = require('underscore'),
  Device = require('../models/devices').Device,
  CommandAPI = require('../api/command-api');


exports.post = function (req, res, next) {
  /**
    POST /devices/:id/commands
    {'command': 'light on'}
  **/

  var id = req.params.id,
    command = req.body.command,
    api;

  api = new CommandAPI.Command();
  api.post(id, command, next);
};

